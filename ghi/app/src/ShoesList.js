import { useEffect, useState } from "react"

function ShoesList() {

  const [shoes, setShoes] = useState([])
  const getData = async ()=> {
    const resp = await fetch('http://localhost:8080/api/shoes')
    if (resp.ok) {
      const data = await resp.json()
      setShoes(data.shoes)
    }
  }



  const handleDelete = async (id) => {
    const resp = await fetch(`http://localhost:8080${id}`, {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
      },
    })

    if (resp.ok) {
      setShoes(shoes.filter(shoe => shoe.id !== id))
    }
  }

  useEffect(() => {
    getData()
  }, [handleDelete()])


    return (
        <table className = "table table-striped table-hover">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Bin Number</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
        {shoes.map(shoe => {
          return (
            <tr key={ shoe.href }>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.model_name }</td>
              <td>{ shoe.color }</td>
              <td>{ shoe.bin.bin_number}</td>
              <td>
                <img src={ shoe.picture_url} className="img-thumbnail"width="50" height="60" />
              </td>
              <td>
                <button onClick={() => handleDelete(shoe.href)}>Delete</button>
              </td>
            </tr>
          );
        })}

        </tbody>
      </table>
    )
}
export default ShoesList
