import React, { useEffect, useState } from 'react';

function ShoeForm () {

    const [bins, setBins] = useState([])

    const [manufacturer, setManufacturer] = useState('')
    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const [modelName, setModelName] = useState('')
    const handleModelNameChange = (event) => {
        const value = event.target.value
        setModelName(value)
    }


    const [color, setColor] = useState('')
    const handleColor = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const [pictureUrl, setPictureUrl] = useState('')
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)

    }

    const [bin, setBin] = useState('')
    const handleBin = (event) => {
        const value = event.target.value
        setBin(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.model_name = modelName
        data.manufacturer = manufacturer
        data.color = color
        data.picture_url = pictureUrl
        data.bin = bin



        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            

            setManufacturer('')
            setModelName('')
            setColor('')
            setPictureUrl('')
            setBin('')
        }

    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setBins(data.bins)
    }
}

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} value = {manufacturer} placeholder="Manufacturer" required type="text" id="manufacturer" className="form-control" name="manufacturer" />
                <label htmlFor="name">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} value={modelName} placeholder="Model Name" required type="text" id="model_name" className="form-control" name="model_name" />
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColor} value={color} placeholder="Color" required type="text" id="color" className="form-control" name="color" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture Url" required type="text" id="picture_url" className="form-control" name="picture_url" />
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={handleBin} value={bin} required id="bin" className="form-select" name="bin">
                  <option value="">Choose a bin</option>
                  {bins.map(bin => {
                    return (
                    <option key={bin.id} value={bin.href}>
                        {bin.bin_number}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ShoeForm;
